@extends('layouts.master')

@section('title', 'Halaman Biodata')
    
@section('content')
<form action="{{route('kirim')}}" method="post">
    @csrf
    <label for="">Nama Depan</label><br>
    <input type="text" name="fname"><br><br>
    <label for="">Nama Belakang</label><br>
    <input type="text" name="lname"><br><br>
    <input type="submit" value="Kirim">
</form>
@endsection
