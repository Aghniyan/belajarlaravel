<?php

use App\Http\Controllers\BiodataController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\IndexController; 
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'utama'])->name('utama');
Route::get('/biodata', [BiodataController::class, 'daftar'])->name('daftar');
Route::post('/kirim', [BiodataController::class, 'kirim'])->name('kirim');

Route::get('/data-table', [IndexController::class, 'dataTable']);